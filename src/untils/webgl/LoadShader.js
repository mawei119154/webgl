export default function LoadShader(Script) {
  //   此函数通过遍历着色器来收集源码
  let Code = "";
  let CurrentChild = Script.firstChild;
  while (CurrentChild) {
    if (CurrentChild.nodeType == CurrentChild.TEXT_NODE) {
      Code += CurrentChild.textContent;
    }
    CurrentChild = CurrentChild.nextSibling;
  }
  console.log(Code)
  return Code;
}
