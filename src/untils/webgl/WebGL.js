import LoadShader from "./LoadShader";
import Cube from "./modelData";
class WebGL {
  constructor(CID, FSID, VSID) {
    this.canvas = document.getElementById(CID);
    this.FShader = document.getElementById(FSID);
    this.VShader = document.getElementById(VSID);
    this.Cube = Cube;
    this._init();
  }
  _init() {
    // 1 设置webgl
    if (
      !this.canvas.getContext("webgl") &&
      !this.canvas.getContext("experimental-webgl")
    ) {
      alert("Your Browser Doesn't Support WebGL");
    } else {
      this.GL = this.canvas.getContext("webgl")
        ? this.canvas.getContext("webgl")
        : this.canvas.getContext("experimental-webgl");
      this.GL.clearColor(1.0, 1.0, 1.0, 1.0);
      // this is the color 清楚颜色就是设置背景颜色
      this.GL.enable(this.GL.DEPTH_TEST);
      // Enable Depth Testing 计算深度
      this.GL.depthFunc(this.GL.LEQUAL);
      // Set Perspective view 透视   其和上面的深度 会让离你近的对象挡住离你远的对象
      this.AspectRatio = this.canvas.width / this.canvas.height;
      // 宽高比

      //
      // 从js 中加载着色器,通过遍历着色器来收集源码
      if (!this.FShader || !this.VShader) {
        alert("Error, Could Not Find Shaders");
      } else {
        // Load and Compile Fragment Shader
        let Code = LoadShader(this.FShader);
        this.FShader = this.GL.createShader(this.GL.FRAGMENT_SHADER);
        this.GL.shaderSource(this.FShader, Code);
        this.GL.compileShader(this.VShader);

        // Load and compild Vertex Shader
        Code = LoadShader(this.VShader);
        this.VShader = this.GL.createShader(this.GL.VERTEX_SHADER);
        this.GL.shaderSource(this.VShader, Code);
        this.GL.compileShader(this.VShader);

        // Create the Shader Program
        this.ShaderProgram = this.GL.createProgram();
        this.GL.attachShader(this.ShaderProgram, this.FShader);
        this.GL.attachShader(this.ShaderProgram, this.VShader);
        this.GL.linkProgram(this.ShaderProgram);
        this.GL.useProgram(this.ShaderProgram);

        // Link Vertex Position Attribute from Shader
        this.VertextPosition = this.GL.getAttribLocation(
          this.ShaderProgram,
          "VertextPosition"
        );
        this.GL.enableVertexAttribArry(this.VertextPosition);

        // Link Texture Coordinate Attribute from Shader
        this.VertextTexture = this.GL.getAttribLocation(
          this.ShaderProgram,
          "TextureCoord"
        );
        this.GL.enableVertexAttribArry(this.VertextTexture);
      }
    }
  }
  draw(Object, Texture) {
    // 3 Draw 函数
    // 绘制函数,绘制过程有很多步骤,最好是每个步骤拆分
    // 将 数据加载到 webgl 的缓存中,然后将缓存连接到着色器中定义的属性,以及变换和透视矩阵
    // 接下来 将纹理 加载到内存中，并且最后调用 draw 绘制

    let VertextBuffer = this.GL.createBuffer();
    // create a new buffer

    this.GL.bindBuffer(this.GL.ARRAY_BUFFER, VertextBuffer);
    // bind it as the Current buffer

    this.GL.bufferData(
      this.GL.ARRAY_BUFFER,
      new Float32Array(Object.Vertices),
      this.GL.STATIC_DRAW
    );
    // fill it with the data

    this.GL.vertextAttribPointer(
      this.VertextPosition,
      3,
      this.GL.FLOAT,
      false,
      0,
      0
    );
    // connect buffer to shader's attribute

    let TextureBuffer = this.GL.createBuffer();
    this.GL.bindBuffer(this.GL.ARRAY_BUFFER, TextureBuffer);
    this.GL.bufferData(
      this.GL.ARRAY_BUFFER,
      new Float32Array(Object.Texture),
      this.GL.STATIC_DRAW
    );
    this.GL.vertextAttribPointer(
      this.VertextTexture,
      2,
      this.GL.FLOAT,
      false,
      0,
      0
    );

    let TriangleBuffer = this.GL.createBuffer();
    this.GL.bindBuffer(this.GL.ARRAY_BUFFER, TriangleBuffer);
    let PerspectiveMatrix = this.MakePerspective(
      45,
      this.AspectRatio,
      1,
      10000.0
    );
    let TransformMatrix = this.MakeTransform(Object);
    this.GL.activeTexture(this.GL.TEXTURE0);
    // set slot 0 as the active texture
    this.GL.bindTexture(this.GL.TEXTURE_2D, Texture);
    // load in the texture to memory
    this.GL.uniform1i(
      this.GL.getUniformLocation(this.ShaderProgram, "uSampler"),
      0
    );
    // update the texture sampler in the fragment shader touse slot 0
    let pmatrix = this.GL.getUniformLocation(
      this.ShaderProgram,
      "PerspectiveMatrix"
    );
    this.GL.uniformMatrix4fv(
      pmatrix,
      false,
      new Float32Array(PerspectiveMatrix)
    );
    let tmatrix = this.GL.getUniformLocation(
      this.ShaderProgram,
      "TransformationMatrix"
    );
    this.GL.uniformMatrix4fv(tmatrix, false, new Float32Array(TransformMatrix));
    // Set The Perspective and Transformation Matrices
    this.GL.drawElements(
      this.GL.TRIANGLES,
      Object.Trinagles.length,
      this.GL.UNSIGNED_SHORT,
      0
    );
    //Draw The Triangles
  }
  MakePerspective(FOV, AspectRatio, Closest, Farest) {
    let YLimit = Closest * Math.tan((FOV * Math.PI) / 360);
    let A = -(Farest + Closest) / (Farest - Closest);
    let B = (-2 * Farest * Closest) / (Farest - Closest);
    let C = (2 * Closest) / (YLimit * AspectRatio * 2);
    let D = (2 * Closest) / (YLimit * 2);
    return [C, 0, 0, 0, 0, D, 0, 0, 0, 0, A, -1, 0, 0, B, 0];
  }
  MakeTransform() {
    return [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, -6, 1];
  }
  LoadTexture(Img) {
    // 4 加载纹理
    //Create a new Texture and Assign it as the active one
    var TempTex = this.GL.createTexture();
    this.GL.bindTexture(this.GL.TEXTURE_2D, TempTex);

    //Flip Positive Y (Optional)
    this.GL.pixelStorei(this.GL.UNPACK_FLIP_Y_WEBGL, true);

    //Load in The Image
    this.GL.texImage2D(
      this.GL.TEXTURE_2D,
      0,
      this.GL.RGBA,
      this.GL.RGBA,
      this.GL.UNSIGNED_BYTE,
      Img
    );

    //Setup Scaling properties
    this.GL.texParameteri(
      this.GL.TEXTURE_2D,
      this.GL.TEXTURE_MAG_FILTER,
      this.GL.LINEAR
    );
    this.GL.texParameteri(
      this.GL.TEXTURE_2D,
      this.GL.TEXTURE_MIN_FILTER,
      this.GL.LINEAR_MIPMAP_NEAREST
    );
    this.GL.generateMipmap(this.GL.TEXTURE_2D);

    //Unbind the texture and return it.
    this.GL.bindTexture(this.GL.TEXTURE_2D, null);
    return TempTex;
  }
}

export default WebGL;
